FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/simple-http-*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
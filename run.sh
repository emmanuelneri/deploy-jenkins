#!/usr/bin/env bash

docker run -it \
   --name jenkins \
   -u root \
   -p 8080:8080 \
   -p 50000:50000 \
   -v /var/run/docker.sock:/var/run/docker.sock \
   -v "$HOME":/home \
   jenkinsci/blueocean
